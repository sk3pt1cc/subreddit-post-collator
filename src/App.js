import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import PostsList from './components/posts-list';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    font-family: 'Lora', sans-serif;
    line-height: 30px;
  }
  
  p {
    font-size: 1.2rem;
  }

  body {
    background-color: black;
    color: #b0b0b0;
  }
`;

const AppWrapper = styled.div`
  width: 60%;
  margin-left: 20%;

  @media only screen and (max-width: 1080px) {
    width: 90%;
    margin-left: 5%;
  }

  @media only screen and (max-width: 450px) {
    width: 98%;
    margin-left: 1%;
  }
`;

const App = () => (
  <AppWrapper>
    <GlobalStyle />
    <PostsList />
  </AppWrapper>
);

export default App;
