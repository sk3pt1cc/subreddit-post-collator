import rp from 'request-promise';
import moment from 'moment';

// This is just for iteration one and we can make this better later by passing in specific
// subreddit and time values.
const apiUrl = 'https://www.reddit.com/r/nosleep/top.json?t=day&limit=10';

const remapPostData = (post) => ({
    title: post.title,
    content: post.selftext.replace(/\n/g, '<br />'), // Replace all newline escapes with html newline tag
    score: post.score,
    createdDate: moment.unix(post.created_utc).format('LT - dddd'),
    link: post.url,
    author: post.author,
});

export const getPosts = async () => {
    const postsResponse = await rp.get(apiUrl);
    const postsArray = JSON.parse(postsResponse).data.children;

    return postsArray.map(post => remapPostData(post.data));
};