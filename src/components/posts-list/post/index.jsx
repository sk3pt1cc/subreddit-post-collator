import React from 'react';
import styled from 'styled-components';
import { FaThumbsUp, FaClock, FaPencilAlt } from "react-icons/fa";

const PostWrapper = styled.div`
    border: 1px solid black;
    padding: 16px;
    .title {
        color: white;
        padding-top: 32px;
        .score {
            font-size: 1rem;
            padding-right: 15px;
            color: lightgreen;
        }

        .created-date {
            font-size: 0.8rem;
            color: burlywood;
            padding-left: 15px;
            @media only screen and (max-width: 1000px) {
                display: block;
                padding: 0;
            }
        }

        h1 {
            line-height: 50px;
        }

        a {
            text-decoration: none;
            color: white;
        }

        .author {
            color: brown;
            padding-left: 5px;
        }
    }
`;

const Post = (props) => (
    <PostWrapper>
        <div className='title'>
            <h1>
                <span className='score'>
                    {props.score} &nbsp; <FaThumbsUp /> 
                </span>
                <a href={props.link}>
                    {props.title}
                </a>
                <span className='created-date'> <FaClock /> &nbsp; {props.createdDate}</span>
            </h1>
            <FaPencilAlt /> &nbsp; by <a className='author' href={`https://www.reddit.com/u/${props.author}`}>u/{props.author}</a>
            <br />
            <br />
        </div>
        <div className='content'>
            <p dangerouslySetInnerHTML={{ __html: props.content }} />
        </div>
    </PostWrapper>
);

export default Post;