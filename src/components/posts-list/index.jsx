import React, { useState, useEffect } from 'react';
import Post from './post';
import * as postsService from '../../helpers/posts-service';

const PostsList = () => {
    const [posts, setPosts] = useState([]);
    
    const getPosts = async () => {
        const receivedPosts = await postsService.getPosts();
        setPosts(receivedPosts);
    }

    useEffect(() => { 
        getPosts();
    }, []);

    return (
        <ul>
            { posts.map(post => <Post key={post.link} {...post} />) }
        </ul>
    );
};

export default PostsList;